"""
utils.py contains misc helper functions the rest of BlipBot2
"""

import json
import discord
import datetime
from config import BaseConfig, MessageBlacklist

base_config = BaseConfig()
message_blacklist = MessageBlacklist()


def add_blacklist(user_id):
    """
    This helper function adds a user id to the defined JSON_CONFIG file
    """

    file = json.load(open(BaseConfig.JSON_CONFIG, 'r'))
    file["blacklist"].append(user_id)

    json.dump(file, open(BaseConfig.JSON_CONFIG, "w"))


def command_check(command_name, message_content):
    """
    This helper function simply checks as an if statement for the commands
    This is better than repeating a long if multipule times in the code
    """

    if message_content.lower().startswith(tuple(prefix + command_name for prefix in base_config.CLIENT_PREFIX)):
        return True
    else:
        return False


def blacklist_check(user):
    """
    This helper function runs to check if the user is in the blacklist
    """

    if user.id in message_blacklist.blacklist:
        embed = embed_generator(
            "Blacklisted!",
            f"Sorry, {user.name} you are blacklisted from utilizing BlipBot!",
            0xe01c11
        )

        return embed


def embed_generator(title, desc, color):
    """
    This helper function generates a simple discord embed
    """

    embed = discord.Embed(color=color)

    embed.add_field(
        name=title,
        value=desc,
        inline=False
    )

    embed.set_footer(text=f"BlipBot, {datetime.datetime.now()}")

    return embed
