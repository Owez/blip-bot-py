# BlipBot2

## Overview

### About

BlipBot2 is the second generation of BlipBot; made with Discord.py in Docker. This bot is designed to be as stable and as low-latency as possible with a good source-base.

### Running

#### Debugging

1. `pip3 install pipenv` to install pipenv
2. `pipenv install` to download dependancies
3. `pipenv shell` to launch the pipenv shell
4. `export x=y` add as many env-vars config.py shows
5. `python bot.py` to run

#### Deployment

Before deploying, make sure you have Docker and docker-compose installed. Production is on a Linux server so it may be diffrent on Windows.

1. `nano blipbot.env` to add lots of env vars as the **Debugging** sections says
2. `sudo docker-compose build` to build the container
3. `sudo docker-compose up -d` to run the container in detached mode (so you can close the terminal)
