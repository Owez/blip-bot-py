"""
config.py is the basic configuration file containing classes to use in BlipBot2
"""

import os
import json


def get_env_var(var_name):
    """
    This helper function simply gets an env var and if it is blank, return nothing
    """

    try:
        return os.environ[var_name]
    except:
        return ""


def get_config_dict(config_name):
    return json.load(open(config_name, 'r'))


class BaseConfig:
    CLIENT_TOKEN = get_env_var("CLIENT_TOKEN")
    JSON_CONFIG = get_env_var("JSON_CONFIG")
    CLIENT_PREFIX = (
        "?",
        "!",
        ".",
        ",",
        ";"
    )


class ChannelConstants(BaseConfig):
    config_dict = get_config_dict(BaseConfig.JSON_CONFIG)

    GUILD_ID = config_dict["guild_id"]
    SUGGEST_CHANNELS = config_dict["suggest_channels"]
    LOG_CHANNELS = config_dict["log_channels"]


class MessageBlacklist(BaseConfig):
    config_dict = get_config_dict(BaseConfig.JSON_CONFIG)

    blacklist = config_dict["blacklist"]
