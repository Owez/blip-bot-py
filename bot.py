# pylint: disable-msg=C0103,W0105
"""
bot.py is the main client file for BlipBot2
"""

import discord
from discord.ext import commands
from config import BaseConfig, ChannelConstants
from utils import add_blacklist, blacklist_check, command_check, embed_generator

base_config = BaseConfig()
channel_constants = ChannelConstants()

client = commands.Bot(base_config.CLIENT_PREFIX)


@client.event
async def on_ready():
    """
    This event runs when the client starts up.
    """

    print(f"{client.user.name} is online with the id of \"{client.user.id}\"!")

    for guild in client.guilds:
        if guild.id not in channel_constants.GUILD_ID:
            embed = embed_generator(
                "Not RFMP!",
                "This bot is not in the RFMP discord! I am leaving automatically..",
                0xe01c11
            )

            await guild.owner.send(embed=embed)
            await guild.leave()


@client.event
async def on_guild_join(guild):
    """
    This event runs when it is added to a guild
    If it isn"t RFMP, it will leave automatically
    """

    if guild.id not in channel_constants.GUILD_ID:
        embed = embed_generator(
            "Not RFMP!",
            "This bot is not in the RFMP discord! I am leaving automatically..",
            0xe01c11
        )

        await guild.owner.send(embed=embed)
        await guild.leave()


@client.event
async def on_member_join(member):
    """
    This event runs when a user joined a whitelisted discord
    It will display a simple intro message
    """

    embed = embed_generator(
        "Welcome to RFMP!",
        "Please read #faq and #rules before continuing!",
        0x3bcc1a
    )

    await member.send(embed=embed)


@client.event
async def on_message(message):
    """
    This event runs when there is a message in RFMP
    """

    if command_check("apply", message.content):
        """
        This is the apply command, it pms the user the Google Form to fill out
        """

        blacklist = blacklist_check(message.author)

        if blacklist:
            await message.author.send(blacklist)

            return

        if message.mentions:
            for user in message.mentions:
                await user.send(f"{message.author.name} sent: https://goo.gl/forms/1xSBvViKHNVzRLjM2")
        else:
            await message.author.send("You sent: https://goo.gl/forms/1xSBvViKHNVzRLjM2")

        await message.channel.send(f"Sent form, {message.author.name}!")
    elif command_check("suggest", message.content):
        """
        This is the suggest command, a user suggest something and it sends to specific channels
        """

        blacklist = blacklist_check(message.author)

        if blacklist:
            await message.author.send(blacklist)
            return

        for channel in client.get_all_channels():
            if channel.id in channel_constants.SUGGEST_CHANNELS:
                embed = embed_generator(
                    f"{message.author.name} suggested",
                    message.content[9:],
                    0x434347
                )

                await channel.send(embed=embed)

        await message.channel.send(f"Sent suggestion, {message.author.name}!")
    elif command_check("blacklist", message.content):
        """
        This is the blacklist command, it allows server admins to blacklist users from entering commands into BlipBot
        It is primarily used to prevent spam of the bot.
        """

        if message.author.guild_permissions.administrator and message.guild.id == channel_constants.GUILD_ID[0]:
            """
            This block runs if the user has got an admin rank and can therefore run the blacklist command
            """

            if message.mentions:
                """
                This block runs if the author mentioned somebody in the blacklist message
                """

                for user in message.mentions:
                    embed = embed_generator(
                        "Blacklisting!",
                        f"Adding {user.name} to the blacklist!",
                        0x000000
                    )

                    await message.channel.send(embed=embed)

                    add_blacklist(message.author.id)
            else:
                """
                This block runs if the user didn't mention anybody in the blacklist message
                """

                embed = embed_generator(
                    "Nobody mentioned!",
                    f"Please mention somebody, {message.author.name}!",
                    0xe01c11
                )

                await message.channel.send(embed=embed)
        else:
            """
            This block runs if the user doesn't have admin rank and tries to run the blacklist command
            """

            embed = embed_generator(
                "Invalid permissions!",
                f"You have not got the sufficant permissions to use this command, {message.author.name}!",
                0xe01c11
            )

            await message.channel.send(embed=embed)
    # elif message.content.startswith(base_config.CLIENT_PREFIX) and message.content not in base_config.CLIENT_PREFIX:
    #     """
    #     This block runs if the command could not be found.
    #     It makes sure it is an actual command, hence the elif
    #     """

    #     embed = embed_generator(
    #         "Command not found!",
    #         f"Sorry, {message.author.name} that command could not be found!",
    #         0xe01c11
    #     )

    #     await message.channel.send(embed=embed)


@client.event
async def on_message_delete(message):
    """
    This block is activated when a message is deleted and simply logs it
    """

    await message_log(
        "Message deleted!",
        f"\'{message.content}\', sent by {message.author.name} ({message.author.id}) ({message.id}) was deleted!",
        0xCC6C6C,
        message,
    )


@client.event
async def on_message_edit(message_before, message_after):
    """
    This block is activated when a message is edited and simply logs it
    """

    if message_after.content and message_before.content != message_after.content:
        await message_log(
            "Message edited!",
            f"\'{message_before.content}\' ({message_before.id}), sent by {message_before.author.name} ({message_before.author.id}) was edited to: \'{message_after.content}\' ({message_after.id})!",
            0x8D6CCC,
            message_before,
        )


async def message_log(title, desc, colour, message):
    """
    This block is the logging system, saving messages to secure channels
    """

    embed = embed_generator(title, desc, colour)

    for channel in client.get_all_channels():
        if (
            channel.id in channel_constants.LOG_CHANNELS
            and message.channel.id not in channel_constants.LOG_CHANNELS
        ):
            await channel.send(embed=embed)

client.run(base_config.CLIENT_TOKEN)
